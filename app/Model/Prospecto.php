<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Prospecto extends Model
{
   protected $table = 'prospectos';
   protected $primaryKey = 'pk';
}

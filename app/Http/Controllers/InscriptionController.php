<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Prospecto;
use App\Model\State;
use App\Model\City;
class InscriptionController extends Controller
{
    /**
     * Show the form to create a new blog post.
     *
     * @return Response
     */
    public function index()
    {
        $allState = State::all();
        $allCity = City::all();
         return view('App/inscription',['states'=>$allState,'citis'=>$allCity]);
    }

    /**
     * Store a new blog post.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'Se Requiere Este Campo!!',
            'numeric' => 'Solo Numeros',
            'date' => 'Formato Incorrecto dd/mm/aaaa',
            'email' => 'Email Invalido',
            'unique' => 'Ya Existe',
            'max'=> 'Solo 160 caracteres'
        ];
       $request->validate([       
        'name' => 'required | max:160',
        'lastName' => 'required | max:160',
        'cell' => 'required | numeric',
        'city' => 'required | max:160',
        'address' => 'required | max:160',
        'tel' => 'required | max:160',
        'email' => 'required | max:160 | email',
        'cc' => 'required | numeric | unique:prospectos',
        'state' => 'required',

       ],$messages);

       $prospecto = new Prospecto;
        $prospecto->name  = $request->name;
        $prospecto->lastName  = $request->lastName;
        $prospecto->cell  = $request->cell;
        $prospecto->city  = $request->city;
        $prospecto->address  = $request->address;
        $prospecto->tel  = $request->tel;
        $prospecto->email  = $request->email;
        $prospecto->cc = $request->cc;
        $prospecto->state = $request->state;
        $prospecto->save();

        return redirect('inscription')->with(['messages'=>'Tu líder de zona te contactara contigo ¡','alerta'=>'alert alert-success']);
    }
}
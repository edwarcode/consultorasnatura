<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('App/home');
});
Route::get('Home', function () {
    return view('App/home');
});
Route::get('/inscription', 'InscriptionController@index');
Route::post('save', 'InscriptionController@store');

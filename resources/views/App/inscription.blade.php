@extends('menu')
@section('title', 'Inscripción')

@section('content')
<section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg-2.jpg');" data-stellar-background-ratio="0.5">
    <div class=" ">
	      <div >
	      	<div class="overlay" ></div>
	        <div class="container-fluid">
	          <div class="row no-gutters slider-text js-fullheight slider-text align-items-end justify-content-start" data-scrollax-parent="true">                  
                @if (session('messages'))
                <div class="col-md-8 align-self-end {{ session('alerta') }}">
                    {{ session('messages') }}
                </div>
                @endif
                <h2>INSCRÍBETE PARA SER CONSULTOR@ NATURA</h2>
                <form action="save" method="POST" class="row one-third align-self-end order-md-last img-fluidcontact-form col-md-8 ">
                    @csrf
                   
                    <div class="form-group container-fluid col-md-6">
                        
                        <input type="text" class="form-control " style=" @error('name') border: 1px solid red; @enderror"  placeholder="Nombre" name="name"  value="{{old('name')}}">
                        <div class="badge badge-danger" >{{ $errors->first('name') }}</div>
                        
                        <input type="text" class="form-control input-lg" style=" @error('lastName') border: 1px solid red; @enderror" placeholder="Apellidos" name="lastName"  value="{{old('lastName')}}">
                        <div class="badge badge-danger">{{ $errors->first('lastName') }}</div>
                        
                        <input type="text" class="form-control input-lg" style=" @error('cc') border: 1px solid red; @enderror"
                                placeholder="Número de Documento" name="cc" value="{{old('cc')}}">
                        <div class="badge badge-danger">{{ $errors->first('cc') }}</div>
                        
                        <select class="custom-select" id="state" name="state" style=" @error('state') border: 1px solid red; @enderror">
                            <option value="">Departamento</option>
                            @foreach ($states as $state)
                                <option value='{{$state->departamento}}' id='{{$state->id}}'>{{$state->departamento}}</option>
                            @endforeach
                        </select>
                        <div class="badge badge-danger">{{ $errors->first('state') }}</div>
                        
                        <select class="custom-select" id="city" name="city" style=" @error('city') border: 1px solid red; @enderror">
                            <option value="">Ciudad</option>
                        </select>
                        <div class="badge badge-danger">{{ $errors->first('city') }}</div>
                        
                    </div>
                    <div class="form-group container-fluid col-md-6">
                            <input type="text" class="form-control input-lg" style=" @error('cell') border: 1px solid red; @enderror"
                                placeholder="Cedula" name="cell" value="{{old('cell')}}">
                            <div class="badge badge-danger">{{ $errors->first('cell') }}</div>
                        
                            <input type="text" class="form-control input-lg"
                                style=" @error('address') border: 1px solid red; @enderror" placeholder="Direccion" name="address"
                                value="{{old('address')}}">
                            <div class="badge badge-danger">{{ $errors->first('address') }}</div>
                        
                            <input type="text" class="form-control input-lg"
                                style=" @error('tel') border: 1px solid red; @enderror" placeholder="Telefono" name="tel"
                                value="{{old('tel')}}">
                            <div class="badge badge-danger">{{ $errors->first('tel') }}</div>
                        
                            <input type="text" class="form-control input-lg"
                                style=" @error('email') border: 1px solid red; @enderror" placeholder="Email" name="email"
                                value="{{old('email')}}">
                            <div class="badge badge-danger">{{ $errors->first('email') }}</div>
                        
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Enviar" class="btn btn-primary py-3 px-5">
                    </div>
                </form>
		          
	        	</div>
	        </div>
	      </div>
      
    </section>
    <input type="hidden" value="{{$citis}}" id="citis"/>
@endsection
@section('script')
<script>
    $('#state').on('change', function () {
        console.log('okokoko', this.value)
        let id = $('#state').find('option:selected')[0].id;
        $('#city').html('').append('<option>Ciudad</option>')
        let cc = JSON.parse($('#citis')[0].value);
        this.value != 'Departamento' && cc.filter(element => {
         element.departamentos_id == id && $('#city').append('<option value="' + element.municipio + '">' + element.municipio + '</option>');
       });  
       
    });
</script>
@endsection
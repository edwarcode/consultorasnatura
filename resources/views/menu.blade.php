<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Natura Colombia</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="images/se-consultora-natura.png">
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="" href="Home"><h2><strong>Natura Colombia</strong></h2></a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	        	<li class="nav-item"><a href="Home" class="nav-link">Home</a></li>
	        	<li class="nav-item"><a href="http://natura.com.vc/evejanpava-e33l8" target="_blank" class="nav-link">Tienda Virtual</a></li>
	        	<li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Revista Digital</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" target="_blank" href="http://natura.com.vc/ciclo_15-7xgip">Ciclo 15</a>
                        <a class="dropdown-item" target="_blank" href="http://natura.com.vc/ciclo_16-2t9y7">Ciclo 16</a>
                        <a class="dropdown-item" target="_blank" href="https://natura.com.vc/ciclo_17-vtbtx">Ciclo 17</a>
                    </div>
                </li>
	        	<li class="nav-item"><a href="#footer" class="nav-link">Contáctanos</a></li>
	          <li class="nav-item"><a href="inscription" class="nav-link">Inscribete</a></li>
	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->
 @yield('content')   
    
@yield('footer')
    <footer class="ftco-footer ftco-section" id="footer">
      <div class="container">
        <div class="row mb-12">
          <div class="col-md-6">
            <div class="ftco-footer-widget mb-6">
              <h2 class="ftco-heading-2 logo">Evelin Pava</h2>
              <p>Quiero invitarte a que sueñes en grande, a qué cumplas esos propósitos que cada año llegaron a tu mente y corazón pero se quedaron solo en planes, conoce este negocio tan maravilloso que te permite disfrutar, aprender y enseñar todo sobre la belleza y el cuidado personal.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="https://api.whatsapp.com/send?phone=573203789450" target="_blank"><span class="icon-whatsapp"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/evelin.pava.752" target="_blank"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="http://instagram.com/yosoynatura_colombia?utm_source=qr" target="_blank"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          
          <div class="col-md-2">
             <div class="ftco-footer-widget mb-2">
              <h2 class="ftco-heading-6">Links</h2>
              <ul class="list-unstyled">
                <li><a href="Home" class="py-2 d-block">Home</a></li>
                <li><a href="http://natura.com.vc/evejanpava-e33l8" target="_blank" class="py-2 d-block">Tienda Virtual</a></li>
                <li class="nav-item dropdown">
                    <a class="py-2 d-block" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Revista Digital</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" style="color: black" target="_blank" href="http://natura.com.vc/ciclo_15-7xgip">Ciclo 15</a>
                        <a class="dropdown-item" style="color: black" target="_blank" href="http://natura.com.vc/ciclo_16-2t9y7">Ciclo 16</a>
                    </div>
                </li>
                <li><a href="#" class="py-2 d-block">Contáctanos</a></li>
                <li><a href="#" class="py-2 d-block">Escribate</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-4">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">¿Tiene preguntas?</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text">Bogotá, Colombia</span></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text">3203789450</span></a></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@yourdomain.com</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved by <a href="edwcode.com" target="_blank">Edwcode</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
@yield('script')    
  </body>
</html>
